-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2020 at 10:02 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_03_25_060113_tasks', 1),
(9, '2020_03_25_061849_rename_name_column', 2),
(11, '2020_03_25_062718_add_last_name_to_users', 3),
(12, '2020_03_25_065833_tasks', 4),
(13, '2020_03_26_123249_add_user_id_to_tasks', 5);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0618ce2f6e29667e8d3d96e3cd2e1d71b7a35d5829fbcc7ab90ee363a400ecdae48b1f8a91de10bf', 6, 1, 'Personal Access Token', '[]', 0, '2020-03-27 00:01:20', '2020-03-27 00:01:20', '2021-03-27 05:31:20'),
('17bb94116d3cdaa4119b2a4e5b3081381b6f38c599cdce37d69d7dc15f775d16d6279eadb20e3a02', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 03:57:43', '2020-03-25 03:57:43', '2020-04-01 09:27:43'),
('197fd20186808b8f5cc2b023fa03aed7025dc7fd3e7de8aa08794ae889f49e8ee3ebfe614902964b', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-26 07:58:44', '2020-03-26 07:58:44', '2021-03-26 13:28:44'),
('5837333537f053dbbd564c465465695903630c091de448ce7f83bcee5365713f0bf7f6ed0853178f', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-26 07:17:55', '2020-03-26 07:17:55', '2021-03-26 12:47:55'),
('5997fd9754470c287ad39d464385596348e8c8d7d2e1b6ebc37132b13cc7c1054e3f822cd27f2a14', 3, 3, 'Personal Access Token', '[]', 0, '2020-03-27 00:53:12', '2020-03-27 00:53:12', '2021-03-27 06:23:12'),
('5ecfd809afbcbfc28142e60f294e74e50a9a89117f72f6383161efb73879cc08af59a890518308e5', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 01:08:48', '2020-03-25 01:08:48', '2020-04-01 06:38:48'),
('68d8a170e2acc48de49c0dd6b7d9656c1e3be88dc02978df8a1e19a3f2a8f23717820ce30d91f86a', 5, 3, 'Personal Access Token', '[]', 0, '2020-03-27 01:06:41', '2020-03-27 01:06:41', '2021-03-27 06:36:41'),
('705bf0d5691dad5a43a59eac18bb57d0be19f5a849196a969e855a2b84be68245ffc5234cae3a34d', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 04:00:03', '2020-03-25 04:00:03', '2020-04-01 09:30:03'),
('749488732364b8709e79572322de358a40b9076b3a382054a0ae8899494d6c33d06286074fb24c45', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 03:58:56', '2020-03-25 03:58:56', '2020-04-01 09:28:56'),
('8636ee372c5decb40375297959bb55ecb25e58ef26b51b67f1ec92ce6490b77e6dc51415482c13db', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-26 07:23:06', '2020-03-26 07:23:06', '2021-03-26 12:53:06'),
('8f4c0716699271679ee49dcbb2526d8390ba95d872af25146b19da7b6f47e0b3bfbb622fd6b027e3', 5, 1, 'Personal Access Token', '[]', 0, '2020-03-26 08:02:50', '2020-03-26 08:02:50', '2021-03-26 13:32:50'),
('91fedd6841f5654df7017deb96e0c899c8fa79b5d7a54db7635186f80814403e50b5b91ef26de47f', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-26 06:52:04', '2020-03-26 06:52:04', '2021-03-26 12:22:04'),
('9b9529822873705714d665c39db8c169fdfbb43113e36ba2199a1b7661495edf35fc5949c15ba8c1', 5, 1, 'Personal Access Token', '[]', 0, '2020-03-26 08:02:38', '2020-03-26 08:02:38', '2021-03-26 13:32:38'),
('b84c53c6a6cfa64bd601f724688ebee479dfbe09e494dc385729b16d71431291d1bc07417438707c', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 04:03:03', '2020-03-25 04:03:03', '2020-04-01 09:33:03'),
('bab0e99c8cb5963915e4227ecad64767bedfc1a0e0cfe52a12d8a2cb610fc0269d9b882686b28894', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-26 07:59:02', '2020-03-26 07:59:02', '2021-03-26 13:29:02'),
('babcb17a8de6fa2f657a95cd5d5c712fab6998d916f6fce12374cc64ffeb2539af4aa125a897f53b', 1, 1, 'Personal Access Token', '[]', 0, '2020-03-26 08:00:05', '2020-03-26 08:00:05', '2021-03-26 13:30:05'),
('dc828032ba5b2802336a27138dd11db6e62db4f24df2dc6b1e74080573efe22049d8ad7bb8125b61', 6, 3, 'Personal Access Token', '[]', 0, '2020-03-27 00:11:31', '2020-03-27 00:11:31', '2021-03-27 05:41:31'),
('ecd47343e947687a9d67ba944573714437e7d5478d8eefcbbd49de1b9986dc56d74f9d11cd43a8ba', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 01:04:23', '2020-03-25 01:04:23', '2020-04-01 06:34:23'),
('ece40db241bb92a03504219254141419fe9ce36f933127f9d187b18f7f508af09e3d101ee9ee7e04', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 01:06:09', '2020-03-25 01:06:09', '2020-04-01 06:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'hMjnRtPstF1ovsQJx5m37ZdkxX54XQV4ufwT5pJb', 'http://localhost', 1, 0, 0, '2020-03-25 00:40:02', '2020-03-25 00:40:02'),
(2, NULL, 'Laravel Password Grant Client', 'I6EFVx31um0Sn3bAZ4SVwHE821oQkmnpjEIjYGOS', 'http://localhost', 0, 1, 0, '2020-03-25 00:40:02', '2020-03-25 00:40:02'),
(3, NULL, 'Laravel Personal Access Client', 'hbI6dfi1E7TArRQC8eryDsz5rkh1d8Xgg1RTX38P', 'http://localhost', 1, 0, 0, '2020-03-27 00:09:34', '2020-03-27 00:09:34'),
(4, NULL, 'Laravel Password Grant Client', 'Tmntw1oO9lr3NDr8YEYLtlnSlgm7TxIPvH5aUqwx', 'http://localhost', 0, 1, 0, '2020-03-27 00:09:34', '2020-03-27 00:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-25 00:40:02', '2020-03-25 00:40:02'),
(2, 3, '2020-03-27 00:09:34', '2020-03-27 00:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_date`, `created_at`, `updated_at`, `user_id`) VALUES
(22, 'hi', '2020-03-25', '2020-03-25 08:11:16', '2020-03-25 08:44:51', '5'),
(24, 'urmidd', '2020-03-26', '2020-03-26 07:19:11', '2020-03-26 07:19:11', NULL),
(25, 'urmishah', '2020-03-26', '2020-03-26 07:21:57', '2020-03-26 07:21:57', NULL),
(26, 'urmishahw', '2020-03-26', '2020-03-26 07:23:16', '2020-03-26 07:23:16', NULL),
(27, 'urmisha', '2020-03-26', '2020-03-26 07:33:45', '2020-03-26 07:33:45', NULL),
(28, 'urmishass', '2020-03-26', '2020-03-26 07:35:22', '2020-03-26 07:35:22', NULL),
(29, 'urmishassa', '2020-03-26', '2020-03-26 07:36:07', '2020-03-26 07:36:07', NULL),
(30, 'urmishassas', '2020-03-26', '2020-03-26 07:37:29', '2020-03-26 07:37:29', NULL),
(31, 'urmishassasw', '2020-03-26', '2020-03-26 07:39:46', '2020-03-26 07:39:46', NULL),
(32, 'urmisz', '2020-03-26', '2020-03-26 07:56:13', '2020-03-26 07:56:13', NULL),
(33, 'urmiszzz', '2020-03-26', '2020-03-26 08:03:59', '2020-03-26 08:03:59', '5'),
(36, 'urmjkjkjxxd', '2020-03-26', '2020-03-26 08:19:44', '2020-03-26 08:19:44', '5'),
(37, 'urmiiii', '2020-03-26', '2020-03-26 08:23:13', '2020-03-26 08:23:13', '37'),
(38, 'urmiiiinn', '2020-03-27', '2020-03-26 23:55:18', '2020-03-26 23:55:18', '5'),
(39, 'food', '2020-03-27', '2020-03-27 00:02:07', '2020-03-27 00:02:07', '6'),
(40, 'gamess', '2020-03-27', '2020-03-27 00:27:19', '2020-03-27 00:27:19', '6'),
(41, 'sports', '2020-03-27', '2020-03-27 00:28:17', '2020-03-27 00:28:17', '6'),
(42, 'zy', '2020-03-27', '2020-03-27 00:54:20', '2020-03-27 00:54:20', '3'),
(43, 'doremon', '2020-03-04', '2020-03-27 01:00:15', '2020-03-27 01:00:15', '3'),
(44, 'ring2', '2020-03-27', '2020-03-27 02:15:43', '2020-03-27 02:47:30', '5'),
(45, 'ss', '2020-03-27', '2020-03-27 03:31:02', '2020-03-27 03:31:02', '5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `last_name`) VALUES
(1, 'uru', '99urmishah@gmail.com', NULL, '$2y$10$ik2nY0Ii9n52TFlTFESMP.c7IZ0KjDfxfJxQw64OITm9dlO011hKu', NULL, '2020-03-25 00:47:17', '2020-03-25 00:47:17', NULL),
(2, 'uru', '99urmishsah@gmail.com', NULL, '$2y$10$WLivs5QInlsSrAoEpmD1OO1g.oSoc5MCFdNc2RcdeRY4VY71IO0qy', NULL, '2020-03-25 00:56:01', '2020-03-25 00:56:01', NULL),
(3, 'uru', '99urmishsSSah@gmail.com', NULL, '$2y$10$1MG8NrcsbRMVJDL89HchCuwyZJKvzBlivPuRUzTkFjW.vSlfC9542', NULL, '2020-03-25 01:02:30', '2020-03-25 01:02:30', NULL),
(4, 'uru', 'shivangshah@gmail.com', NULL, '$2y$10$pIfNdG6ok4Z.hpN.UXtKReAmSWzW94u0uboGGhZbCUFW5JPv4ab2S', NULL, '2020-03-25 03:58:09', '2020-03-25 03:58:09', 'shaH'),
(5, 'urmi d', 'urushah@gmail.com', NULL, '$2y$10$R0Km28LBVqiHS64l/DKYLOS/62toBP5OoU..hl86TbYn4u7LhPQYq', NULL, '2020-03-26 08:02:23', '2020-03-26 08:02:23', 'shaH'),
(6, 'Disha', 'urmishah@gmail.com', NULL, '$2y$10$yIqRNPlHT2Knxt6wM51L5.Tk61zvdIByoi1UWHD5dUYs0jlfhiQU.', NULL, '2020-03-27 00:00:58', '2020-03-27 00:00:58', 'roy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
