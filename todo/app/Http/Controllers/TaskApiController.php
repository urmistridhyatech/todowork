<?php

namespace App\Http\Controllers;
use App\Task;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;
class TaskApiController extends Controller //this controller is for CRUD of task
{

	/* = Created Date: 25-03-2020
     = it returns all the available tasks from the database
     	and returns the response in json 
     	if any records are not available it returns 404
   ------------------------------------------------------ */
   public function getAllTasks() 
  {
    	// logic to get all tasks
       if(Auth::guard('api')->user())
      {
      $user_id = auth()->guard('api')->user()->id;

    	$tasks = Task::where('user_id',$user_id)->get();

               	 if($tasks->isEmpty())
               	{
               	 	return response()->json([
               	 	"success" => false,
                    	"message" => "Task not found",
                  ], 404);
               	}
               	 else
               	{
              	 	   $tasks = Task::where('user_id',$user_id)->get();
             			 return response()->json([
             		 	"success"=> true,
          			"message"=>"all tasks are fetched",
             		 	"data" => $tasks
             		 ],200);

          		  }	
       }
      else
      {
               return response()->json([
                "success"=> false,
                "message"=>"unauthorized to access",
                 
                 ],401);
       }
  }
    




  /* = Created Date: 25-03-2020
   = it creates a new task if the task is unique
   ------------------------------------------------------ */
    public function createTask(Request $request) 
    {
      
  // logic to create a task record goes here
      if(Auth::guard('api')->user())
      {
          $user_id = auth()->guard('api')->user()->id;

            $validator = Validator::make($request->all(), [
            'task_name' => 'required|unique:tasks',
           ]);

                if ($validator->fails())
                {
                    $response = [
                        'success' => false,
                        'data' => 'Validation Error.',
                        'message' => $validator->errors()
                    ];
                    return response()->json($response, 404);
                }
       
                else
                {
                  $task= new Task;

                  $todaydate = date('Y-m-d');
              		$task->task_name = $request->task_name;
              		$task->task_date = $todaydate;
                  $task->user_id = $user_id;

              		$task->save();

              		return response()->json([
              		   	"success" =>true,
                        		
              		    "message" => "task created"
              		    ], 201);

                 }

      }
      else
      {
      return response()->json([
        "success" =>false,
              
        "message" => "not authenticate user"
        ], 401);
       }

	}
    



  	/* = Created Date: 25-03-2020
     = it fetces a task whose id is matched with the parameter
   ------------------------------------------------------ */

    public function getTask($id) 
    {
      // logic to get a task record goes here


       if(Auth::guard('api')->user())
      {
    	$user_id = auth()->guard('api')->user()->id;

    	if (Task::where('id', $id)->where('user_id',$user_id)->exists())
    	 {
                $task= Task::where('id', $id)->get();
                return response()->json([
           		 	"success"=> true,
        			   "message"=>"task is fetched",
           		 	 "data" => $task
           		 ],200);
      	} 
     	else
     	{
        return response()->json([
        	"success"=>false,
          "message" => "Task not found"
        ], 404);
      }
    }
    else
    {
      return response()->json([
          "success"=>false,
          "message" => "unauthorized access"
        ], 401);
    }

    }
    


  	/* = Created Date: 25-03-2020
     = it updates the task whose id is matched with parameter
   ------------------------------------------------------ */
public function updateTask(Request $request, $id) 
    {
      // logic to update a task record goes here
    	      
      if(Auth::guard('api')->user())
      {         
                  $user_id = auth()->guard('api')->user()->id;
                 $task= new Task;
              	if (Task::where('id', $id)->where('user_id',$user_id)->exists())
              	 {
                  $task = Task::find($id);
                  $todaydate = date('Y-m-d');
                  $task->task_name = is_null($request->task_name) ? $task->task_name : $request->task_name;
                  $task->task_date = $todaydate;
                  $task->save();

                  return response()->json([
                    "message" => "records updated successfully"
                  ], 200);
                	} 
      
                  else
                   {
                    return response()->json([
                      "message" => "Task not found"
                    ], 404);
                  }
        }
        else
        {
              return response()->json([
              "success"=> false,
              "message"=>"unauthorized to access",
               
               ],401);
        }
    }





  	/* = Created Date: 25-03-2020
     = it deletes task with id ,matched with parameter id
   ------------------------------------------------------ */
public function deleteTask ($id)
 {
      // logic to delete a task record goes here
 if(Auth::guard('api')->user())
      {
       $user_id = auth()->guard('api')->user()->id;
            	 if(Task::where('id', $id)->where('user_id',$user_id)->exists())
            	{
                      $task = Task::find($id);
                      $task->delete();

                      return response()->json([
                      	"success"=>true,
                        "message" => "records deleted"
                      ], 202);
              	} 
              	else
               {
                    return response()->json([
                    	"success" =>false,
                      "message" => "Task not found"
                    ], 404);
              }
    }
    else
      {
        return response()->json([
          "success" =>false,
          "message" => "unauthorized access"
        ], 401);
      }

}



  /* = Created Date: 25-03-2020
     =Task number 5, fetching task list from selected date
     
   ------------------------------------------------------ */
public function getTaskByDate(Request $request) 
{
      // logic to get all tasks
    if(Auth::guard('api')->user())
      {
            $user_id = auth()->guard('api')->user()->id;

            $task_date=$request->task_date;

            $tasks = Task::where('user_id',$user_id)->get();

               if($tasks->isEmpty())
              {
                return response()->json([
                "success" => false,
                    "message" => "Task not found",
                ], 404);
              }
              else
              {
                      $tasks = Task::where('user_id',$user_id)->where('task_date',$task_date)->get();

                     if($tasks->isEmpty())
                     {
                            return response()->json([
                            "success" => false,
                                "message" => "Task not found",
                            ], 404);
                     }

                    else
                    {
                                 return response()->json([
                                "success"=> true,
                              "message"=>"all tasks are fetched",
                                "data" => $tasks
                               ],200);
                      }
               } 
      }
  else

  {
           return response()->json([
              "success"=> false,
            "message"=>"unauthorized to access",
             
             ],401);
  }


    }
}

