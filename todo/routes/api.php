<?php
use Illuminate\Http\Request;
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::get('Tasks', 'TaskApiController@getAllTasks');
Route::get('Tasks/{id}', 'TaskApiController@getTask');
Route::get('TaskByDate', 'TaskApiController@getTaskByDate');

Route::post('Tasks', 'TaskApiController@createTask');
Route::put('Tasks/{id}', 'TaskApiController@updateTask');
Route::delete('Tasks/{id}','TaskApiController@deleteTask');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
