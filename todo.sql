-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2020 at 11:53 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_03_25_060113_tasks', 1),
(9, '2020_03_25_061849_rename_name_column', 2),
(11, '2020_03_25_062718_add_last_name_to_users', 3),
(12, '2020_03_25_065833_tasks', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('17bb94116d3cdaa4119b2a4e5b3081381b6f38c599cdce37d69d7dc15f775d16d6279eadb20e3a02', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 03:57:43', '2020-03-25 03:57:43', '2020-04-01 09:27:43'),
('5ecfd809afbcbfc28142e60f294e74e50a9a89117f72f6383161efb73879cc08af59a890518308e5', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 01:08:48', '2020-03-25 01:08:48', '2020-04-01 06:38:48'),
('705bf0d5691dad5a43a59eac18bb57d0be19f5a849196a969e855a2b84be68245ffc5234cae3a34d', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 04:00:03', '2020-03-25 04:00:03', '2020-04-01 09:30:03'),
('749488732364b8709e79572322de358a40b9076b3a382054a0ae8899494d6c33d06286074fb24c45', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 03:58:56', '2020-03-25 03:58:56', '2020-04-01 09:28:56'),
('b84c53c6a6cfa64bd601f724688ebee479dfbe09e494dc385729b16d71431291d1bc07417438707c', 3, 1, 'Personal Access Token', '[]', 1, '2020-03-25 04:03:03', '2020-03-25 04:03:03', '2020-04-01 09:33:03'),
('ecd47343e947687a9d67ba944573714437e7d5478d8eefcbbd49de1b9986dc56d74f9d11cd43a8ba', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 01:04:23', '2020-03-25 01:04:23', '2020-04-01 06:34:23'),
('ece40db241bb92a03504219254141419fe9ce36f933127f9d187b18f7f508af09e3d101ee9ee7e04', 3, 1, 'Personal Access Token', '[]', 0, '2020-03-25 01:06:09', '2020-03-25 01:06:09', '2020-04-01 06:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'hMjnRtPstF1ovsQJx5m37ZdkxX54XQV4ufwT5pJb', 'http://localhost', 1, 0, 0, '2020-03-25 00:40:02', '2020-03-25 00:40:02'),
(2, NULL, 'Laravel Password Grant Client', 'I6EFVx31um0Sn3bAZ4SVwHE821oQkmnpjEIjYGOS', 'http://localhost', 0, 1, 0, '2020-03-25 00:40:02', '2020-03-25 00:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-25 00:40:02', '2020-03-25 00:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_date`, `created_at`, `updated_at`) VALUES
(4, 'angulaas', '9999-12-11', '2020-03-25 03:17:42', '2020-03-25 03:17:42'),
(5, 'xx', '9999-12-11', '2020-03-25 03:18:01', '2020-03-25 03:18:01'),
(6, '11', '9999-12-11', '2020-03-25 03:18:11', '2020-03-25 03:18:11'),
(7, 's', '9999-12-11', '2020-03-25 03:19:39', '2020-03-25 03:19:39'),
(8, '1111', '9999-12-11', '2020-03-25 03:19:44', '2020-03-25 03:19:44'),
(9, '1111', '9999-12-11', '2020-03-25 03:32:23', '2020-03-25 03:32:23'),
(10, '1111', '9999-12-11', '2020-03-25 03:36:45', '2020-03-25 03:36:45'),
(11, 'shshh', '9999-12-11', '2020-03-25 03:36:54', '2020-03-25 03:36:54'),
(12, 'skajlk', '9999-12-11', '2020-03-25 03:37:33', '2020-03-25 03:37:33'),
(13, '11', '9999-12-11', '2020-03-25 03:38:42', '2020-03-25 03:38:42'),
(14, '11', '9999-12-11', '2020-03-25 03:39:32', '2020-03-25 03:39:32'),
(15, '167', '9999-12-11', '2020-03-25 03:45:44', '2020-03-25 03:45:44'),
(16, '167s', '9999-12-11', '2020-03-25 03:51:39', '2020-03-25 03:51:39'),
(17, 'shah', '2033-12-12', '2020-03-25 04:05:32', '2020-03-25 04:05:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `last_name`) VALUES
(1, 'uru', '99urmishah@gmail.com', NULL, '$2y$10$ik2nY0Ii9n52TFlTFESMP.c7IZ0KjDfxfJxQw64OITm9dlO011hKu', NULL, '2020-03-25 00:47:17', '2020-03-25 00:47:17', NULL),
(2, 'uru', '99urmishsah@gmail.com', NULL, '$2y$10$WLivs5QInlsSrAoEpmD1OO1g.oSoc5MCFdNc2RcdeRY4VY71IO0qy', NULL, '2020-03-25 00:56:01', '2020-03-25 00:56:01', NULL),
(3, 'uru', '99urmishsSSah@gmail.com', NULL, '$2y$10$1MG8NrcsbRMVJDL89HchCuwyZJKvzBlivPuRUzTkFjW.vSlfC9542', NULL, '2020-03-25 01:02:30', '2020-03-25 01:02:30', NULL),
(4, 'uru', 'shivangshah@gmail.com', NULL, '$2y$10$pIfNdG6ok4Z.hpN.UXtKReAmSWzW94u0uboGGhZbCUFW5JPv4ab2S', NULL, '2020-03-25 03:58:09', '2020-03-25 03:58:09', 'shaH');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
